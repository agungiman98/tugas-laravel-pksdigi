<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@home');

Route::get('/laravel', 'AuthController@laravel');
Route::get('/register', 'AuthController@register');
Route::post('/welcome', 'AuthController@kirim');
Route::get('/data-tables', 'AuthController@data');
Route::get('/master', 'AuthController@master');

