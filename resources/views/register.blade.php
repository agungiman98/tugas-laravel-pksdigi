@extends('layouts.master')
@section('title', 'Register')
@section('content')
    <div class=" form">
      <div class="container-fluid">
        <h3>Buat Account Baru</h3>
        <h4>Sign Up Form</h4>
        <div class="row">
          <div class="col-lg-6 col-md-12 col-sm-12 col-12">
            <div class="card">
              <div class="card-body">
                <form action="/welcome" method="post">
                  @csrf

                  <div class="row">
                    <div class="col-6">
                      <label for="fname">First Name:</label><br />
                      <input type="text" class="form-control" placeholder="first name" id="fname" name="fname"/>
                    </div>
                    <div class="col-6">
                      <label for="lname">Last Name:</label><br />
                      <input type="text" class="form-control" placeholder="last name" id="lname" name="lname" />
                    </div>
                  </div>
                 <div class="mt-4">
                  <p>Gender:</p>
                  <input type="radio" id="Male" name="gender" />
                  <label for="Male">Male</label>
                  <input type="radio" id="Female" name="gender" />
                  <label for="Female">Female</label>
                </div>
                  <br />
                  <label for="Nationality">Nationality</label>
                  <br />
                  <select name="Nationality" class="custom-select" id="Nationality">
                    <option value="Indonesia">Indonesia</option>
                    <option value="Amerika">Amerika</option>
                    <option value="Arab">Arab</option></select
                  ><br /><br />
                  <label for="lang">Language Spoken</label>
                  <br />
                  
                    <input type="checkbox"  name="Bahasa Indoensia" id="bind" />
                    <label for="bind" >Bahasa Indonesia</label><br />
                    <input type="checkbox"  name="English" id="eng" />
                    <label for="eng" >English</label><br />
                    <input type="checkbox"  name="Other" id="other" />
                    <label for="other" >Other</label><br /><br />
                 
                  <label for="Bio">Bio</label><br />
                  <textarea name="Bio" class="form-control"  id="Bio" cols="30" rows="10"></textarea><br />
                  <input type="submit" value="Kirim" />
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

@endsection