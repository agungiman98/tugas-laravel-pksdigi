@extends('layouts.master')
@section('title', 'Home')
@section('content')
{{-- <!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Tugas Pekan 1 hari 1</title>
  </head> --}}
  <body>
    <div class="container">
      <h1>Media Online</h1>
      <br />
      <h2>Sosial Media Developer</h2>
      <br />
      <p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
      <br />
      <h2>Benefit Join di Media Online</h2>
      <br />
      <ul>
        <li><p>Mendapatkan motivasi dari sesama para Developer</p></li>
        <li><p>Sharing Knowledge</p></li>
        <li><p>Dibuat oleh calon web developer terbaik</p></li>
      </ul>
      <br />
      <h2>Cara Bergabung ke Media Online</h2>
      <ol>
        <li><p>Mengunjungi Website ini</p></li>
        <li>
          <p>Mendaftarkan di <a href="/register">Form Sign Up</a></p>
        </li>
        <li>Selesai</li>
      </ol>
    </div>
  </body>
{{-- </html> --}}
@endsection