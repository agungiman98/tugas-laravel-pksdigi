<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('register');
    }

    public function kirim(Request $request)
    {
        $fname = $request['fname'];
        $lname = $request['lname'];

        return view('welcome', compact('fname', 'lname'));
    }

    public function data()
    {
        return view('data-tables');
    }

    public function master()
    {
        return view('layouts.master');
    }
    public function laravel()
    {
        return view('laravel');
    }
}
